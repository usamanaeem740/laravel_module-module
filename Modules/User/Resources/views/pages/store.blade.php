   @extends('user::layouts.master')

   @section('content')
    <div class="container">
        <div class="row gutters">
            <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="account-settings">
                            <div class="user-profile">
                                <div class="user-avatar">
                                    <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="Maxwell Admin">
                                </div>
                                <h5 class="user-name">Yuki Hayashi</h5>
                                <h6 class="user-email">yuki@Maxwell.com</h6>
                            </div>
                            <div class="about">
                                <h5 class="mb-2 text-primary">About</h5>
                                <p>I'm Yuki. Full Stack Designer I enjoy creating user-centric, delightful and human experiences.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form  class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12" method="post" 
            action="{{route('user.store')}}">
            @csrf
            <div>
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row gutters">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h6 class="mb-3 text-primary">Personal Details</h6>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="fullName">Full Name</label>
                                    <input type="text" name="name" 
                                    class="form-control @error('name') is-invalid @enderror" value="{{old('name' ,$user->name)}}"   
                                    id="fullName" placeholder="Enter full name">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                          <strong >{{ $message }} </strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="eMail">Email</label>
                                    <input  name="email" 
                                    class="form-control 
                                    @error('email') is-invalid @enderror"
                                    value="{{old('email' ,$user->email)}}" id="eMail" placeholder="Enter email ID">
                                     @error('email')
                                        <span class="invalid-feedback" role="alert">
                                          <strong >{{ $message }} </strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="number" name="phone" 
                                    class="form-control 
                                    @error('phone') is-invalid @enderror"
                                    value="{{old('phone' ,$user->phone)}}"
                                     id="phone" placeholder="Enter phone number">
                                     @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                          <strong >{{ $message }} </strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="website">Website URL</label>
                                    <input type="url" name="website_url" class="form-control" value="{{old('website_url' ,$user->website_url)}}"
                                    id="website" placeholder="Website url">
                                </div>
                            </div>
                        </div>
                        <div class="row gutters">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h6 class="mb-3 text-primary">Address</h6>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="Street">Street</label>
                                    <input type="name" name="street" 
                                    class="form-control  @error('street') is-invalid @enderror" 
                                    value="{{old('street' ,$user->street)}}"
                                    id="Street" placeholder="Enter Street">
                                    @error('street')
                                        <span class="invalid-feedback" role="alert">
                                          <strong >{{ $message }} </strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="ciTy">City</label>
                                    <input type="name" name="city" 
                                    class="form-control  @error('street') is-invalid @enderror" 
                                    value="{{old('city' ,$user->city)}}"
                                    id="ciTy" placeholder="Enter City">
                                    @error('city')
                                        <span class="invalid-feedback" role="alert">
                                          <strong >{{ $message }} </strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="sTate">State</label>
                                    <input type="text" name="state" 
                                    class="form-control @error('state') is-invalid @enderror" alue="{{old('state' ,$user->state)}}"
                                    value="{{old('state' ,$user->state)}}"
                                    id="sTate" placeholder="Enter State">

                                    @error('state')
                                        <span class="invalid-feedback" role="alert">
                                          <strong >{{ $message }} </strong>
                                        </span>
                                    @enderror


                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="zIp">Zip Code</label>
                                    <input type="number" name="zip_code" 
                                    class="form-control @error('zip_code') is-invalid @enderror" 
                                    value="{{old('zip_code' ,$user->zip_code)}}"
                                    id="zIp" placeholder="Zip Code">
                                    @error('state')
                                        <span class="invalid-feedback" role="alert">
                                          <strong >{{ $message }} </strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row gutters">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="text-right">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
            </form>
            
        </div>
    </div>


    @endsection